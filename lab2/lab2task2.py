"""Lab 2 Task 2
This module contains functions for simulating Brownian motion
and analyzing the results
"""
import numpy as np
import matplotlib.pyplot as plt
import math

def brown1(Nt,M,dt=0.0001):
    """Run M Brownian motion simulations each consisting of Nt time steps
    with time step = dt
    Returns: X: the M trajectories; Xm: the mean across these M samples; Xv:
    the variance across these M samples
    """
    from numpy.random import randn

    #Initialize variable
    X = np.zeros((M,Nt+1))

    #1D Brownian motion: X_j+1 = X_j + sqrt(dt)*N(0,1)
    for i in range(M):
        for j in range(Nt):
            X[i,j+1] = X[i,j] + np.sqrt(dt)*randn(1)

    Xm = X.mean(axis=0)
    Xv = X.var(axis=0)

    return X, Xm, Xv

def brown2(Nt, M, dt=0.0001):
    from numpy.random import randn



    #1D Brownian motion: X_j+1 = X_j + sqrt(dt)*N(0,1)
    N = np.sqrt(dt) * randn(M, Nt + 1)
    X = np.cumsum(N, axis=1)
    Xm = X.mean(axis=0)
    Xv = X.var(axis=0)

    return X, Xm, Xv


def analyze(Mvalues, Nt=100, dt = 0.0001, display=False):
    """Complete this function to analyze simulation error
    """
    var_list = []
    eps_list = []
    for M in Mvalues:
        X, Xm, Xv = brown2(Nt, M, dt=dt)
        var = X[:,100].var()
        var_list.append(var)
        eps = abs(100 * dt - var)
        eps_list.append(eps)
    if display:
        log_eps_list = [math.log(i) for i in eps_list]
        log_Mvalues = [math.log(i) for i in Mvalues]
        plt.figure()
        plt.title("log log plot of epsilon against M")
        plt.xlabel("log(M)")
        plt.ylabel("log(eps)")
        plt.plot(log_Mvalues, log_eps_list)
        plt.savefig('error_plot.png')
        
        # do a polyfit
    return(Mvalues, var_list, eps_list)
        


if __name__ == '__main__':
    Nt = 100
    dt = 0.0001
    M = 20
    t = np.linspace(0,Nt*dt, Nt+1)
    X, Xm, Xv = brown2(Nt, M, dt=dt)
    
    plt.figure()
    plt.plot(t, X[0,:])
    plt.plot(t,X[1,:])
    
    plt.figure()
    plt.plot(t, Xm)
    
    plt.figure()
    plt.plot(t, Xv**0.5)
    
    M = 200
    X, Xm, Xv = brown2(Nt, M, dt=dt)
    plt.figure()
    plt.plot(t, Xm)
    
    plt.figure()
    plt.plot(t, Xv**0.5)
    
    Mvalues = [10, 100, 1000, 5000, 10000, 20000, 50000]
    analyze(Mvalues, display=True)
    
    """
    We see that as log(eps) decreases from -5 to -10, log(M) increases from 
    approximately 2 to 12. If epsilon decreases approximately as 1/sqrt(M), 
    then we would expect log(epsilon) to decreases as -0.5log(M). So on this
    log-log plot, we would expect to see approximately a straight line with 
    gradient equal to -0.5. As we can see from the figure, this is roughly what
    we have. 
    """
    