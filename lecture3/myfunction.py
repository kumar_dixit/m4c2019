def sqrt_fn(a, n, x0=1, tol=1.0e-14, debug=False, return_delta=False):

    """
    a - number to square root
    n - number of iterations
    x0 - initial guess
    tolerance - stop if current guess is within tolerance of answer
    """
    delta_list = []
    assert type(a) is int or type(a) is float, "error, input must be numeric"
    assert a >= 0, "error, a must be non-negative"
    for i in range(n):
        x = x0/2 + a/(2*x0)
        if debug:
            print("On iteration %d, x = %24.16f" % (i, x0))
        delta = abs(x - x0)
        delta_list.append(delta)
        if delta < tol:
            if debug:
                print("converged")
            break
        x0 = x
    if return_delta:
        return(delta_list)
    return(x)


def test_sqrt(avalues=[0.031, 584838, 0.48385848, 0.5858, 547272982829],
              tol=1e-14, n=50):
    """
    test sqrt_fn with few values
    """
    from math import sqrt
    for a in avalues:
        s = sqrt(a)
        s2 = sqrt_fn(a, n, tol=tol)
        delta_s = abs(s2 - s)
        print("a = %f, delta_s = %24.16f" % (a, delta_s))
        assert delta_s < tol, "Error, sqrt_fn fails for a=%f" % a
    print("all tests ok")


def test_convergence(a, delta_list):
    """
    a - number we are square rooting
    delta - vector of deltas across each iteration
    """
    for element in delta_list:
        pass

if __name__ == '__main__':
    test_sqrt()
    
