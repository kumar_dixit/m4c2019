! Lab 4, task 1

! To compile this code:
! $ gfortran -o task1.exe lab41.f90
! To run the resulting executable: $ ./task1.exe

subroutine computeSum(N, display_iteration, odd_sum)
  implicit none
  integer :: i1
  integer, intent(in) :: N, display_iteration
  real(kind=8), intent(out) :: odd_sum
  odd_sum = 0.d0
  do i1 = 1, N
    odd_sum = dble(odd_sum) + (2.d0 * dble(i1) - 1.d0)
    if (display_iteration == 1) then
      print *, 'sum=', odd_sum
    end if
  end do
end subroutine computeSum

program task1
  implicit none
  integer, parameter :: N=5, display_iteration = 1
  real(kind=8) :: odd_sum
  call computeSum(N, display_iteration, odd_sum)
  print *, 'sum=', odd_sum
end program task1
