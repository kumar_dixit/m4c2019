#Assumes lab5 fortran code has been compiled with: $ f2py -c lab5.f90 -m l5
import numpy as np
import matplotlib.pyplot as plt
from l5 import quad

#set parameters, initialze arrays
nvalues = [6,12,24,48]
I_mid = np.zeros(len(nvalues))
I_sim = np.zeros(len(nvalues))

#compute integrals with different resolutions
for i,n in enumerate(nvalues):
    # Compute integrals with n intervals with each method, storing the results
    # in I_mid and I_sim
    quad.quad_n = n
    quad.midpoint()
    I_mid[i] = quad.quad_sum
    quad.simpson()
    I_sim[i] = quad.quad_sum

#Plot errors on log-log plots
mid_Error = np.abs(np.pi - I_mid)
sim_error = np.abs(np.pi - I_sim)

plt.figure()
plt.loglog(nvalues, mid_Error, label="Midpoint")
plt.loglog(nvalues, sim_error, label="Simpson's")
plt.xlabel("log n")
plt.ylabel("log error")
plt.title("Log error against log n")
plt.legend()
